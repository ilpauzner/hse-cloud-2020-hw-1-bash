#!/bin/sh

read -r n
total=0

for i in $(seq 1 "$n");
do
  read -r number
  total=$((total + number))
done

echo "$total" / "$n" | bc -l | LC_ALL='en_US.UTF-8' awk '{ printf "%.3f\n", $0 }'



#!/bin/bash

read -r a
read -r b
read -r c

if [ $a == $b ] && [ $b == $c ] && [ $c == $a ]; then
  printf "EQUILATERAL"
elif [ $a == $b ] || [ $b == $c ] || [ $c == $a ]; then
  printf "ISOSCELES"
else
  printf "SCALENE"
fi

#!/bin/bash

while read -r line; do
  my_array=("${my_array[@]}" "$line")
done

for item in ${my_array[*]}; do
  if ! { [[ $item =~ a ]] || [[ $item =~ A ]]; }; then
    echo "$item"
  fi
done

#!/bin/bash

while read -r line; do
  my_array=("${my_array[@]}" "$line")
done

for item in ${!my_array[*]}; do
  my_array[$item]=$(echo "${my_array[$item]}" | sed 's/[A-Z]/./')
done

echo "${my_array[*]}"

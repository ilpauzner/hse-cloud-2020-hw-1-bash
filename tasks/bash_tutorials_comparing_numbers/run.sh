#!/bin/sh

read -r a
read -r b

if [ "$a" -gt "$b" ]; then
  printf "X is greater than Y"
fi
if [ "$a" -eq "$b" ]; then
  printf "X is equal to Y"
fi
if [ "$a" -lt "$b" ]; then
  printf "X is less than Y"
fi

#!/bin/sh

read -r line
echo "$line" | bc -l | LC_ALL='en_US.UTF-8' awk '{ printf "%.3f\n", $0 }'
#!/bin/bash

read -r line
if [ "$line" == "y" ] || [ "$line" == "Y" ]; then
  printf "YES"
else
  printf "NO"
fi
